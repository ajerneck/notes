{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# OPTIONS_GHC -fno-cse #-}

module Main where

import           Lib

import           Data.Map               as M
import           Data.Text              as T
import           Data.Text.IO           as TIO
import           System.Console.CmdArgs (Data, Typeable, auto, cmdArgs, def,
                                         help, modes, (&=))

newtype Config = Config {
  notesFile :: FilePath
  } deriving Show

cfg = Config "notes.txt"

data N = Add {title    :: String
             , content :: String
             , url     :: String}
       | List
       deriving (Show, Data, Typeable)

addMode = Add { Main.title = def &= help "note title"
            , Main.content = def &= help "note content"
            , Main.url = def &= help "note url"
            } &= help "Add a note"
listMode = List &= auto

main :: IO ()
main = do
  args <- cmdArgs (modes [addMode, listMode])
  notes <- readNotes (notesFile cfg)
  program args notes

program :: N -> Notes -> IO ()

program (Add t c u) ns = do
  let nss = addNote ns (Note (T.pack t) (T.pack c) (T.pack u))
  saveNotes nss (notesFile cfg)

program List ns = do
  mapM_ (TIO.putStrLn . prettyPrint) (noteMap ns)
