# notes

simple program for saving little notes.

# use cases

add a note about something you want to read. Then it should have a feature to launch the url.

tag notes

search notes full text

set reminders to notes

show some tags as statuses, ie, todo, in progress, done

automatically organize notes into themes.
this is the most useful one.
themes are implemented as tags as well. 
categorize based on content, but, also on the content on the url.

make a nice web interface

# current work

next is using the state monad for the notes

# todo

speed up ci by using cached docker images


add config file, maybe using [conferer](https://blog.10pines.com/2021/03/02/conferer-a-configuration-library-for-haskell/)

probably need to use a monad to keep everything in.


# done

pretty print notes
speed up ci by running test before build, no need to build if tests fail

add command line commands using [cmdargs](https://hackage.haskell.org/package/cmdargs)
