{-# LANGUAGE OverloadedStrings #-}
import Test.Hspec

import Data.Map as M
import qualified Data.Text as T
import System.Directory (removeFile)
import System.IO.Temp (emptyTempFile)

import Lib

mkNote :: T.Text -> T.Text -> T.Text -> (Note, Notes)
mkNote t c u = (n, nss) where
  ns = initNotes
  n = Note t c u
  nss = addNote ns n


main :: IO ()
main = hspec $ do
  describe "addNote" $ do
    it "adds a note" $ do
      let (n, (Notes k nss)) = mkNote "title" "some text" "example.com"
      M.member (k-1) nss `shouldBe` True

      title n `shouldBe` "title"
      content n `shouldBe` "some text"
      url n `shouldBe` "example.com"

    it "adds only a note" $ do
      let (n, ns) = mkNote "title" "some text" "example.com"
      ns `shouldBe` (Notes 1 (fromList [(0, n)]))

  describe "deleteNote" $ do
    it "deletes a note" $ do
      let ns = initNotes
      let n = Note "title" "some text" "example.com"
      let ns2 = addNote ns n

      let ns3 = deleteNote ns2 (nextKey ns2 - 1)
      ns3 `shouldBe` ns

  describe "saveNotes and readNotes" $ do
    it "saves and reads notes to/from disk" $ do
      f <- emptyTempFile "." ".test-notes.txt"
      let ns = initNotes
      let n = Note "title" "some text" "example.com"
      let nss  = addNote ns n
      saveNotes nss f
      ns3 <- readNotes f
      nss `shouldBe` ns3
      removeFile f

  describe "readNote" $ do
    it "handles non-existent file" $ do
      ns2 <- readNotes "non-existent-file"
      ns2 `shouldBe` (initNotes)


  describe "prettyPrint" $ do
    it "returns text with the right separators" $ do

      let n = Note "title" "some text" "example.com"
      let pp = prettyPrint n

      ("------" `T.isInfixOf` pp) `shouldBe` True


  describe "getNote" $ do
    it "returns the right note" $ do
      let (n, ns@(Notes k _)) = mkNote "title" "some text" "example.com"

      getNote (k - 1) ns `shouldBe` (Just n)

    it "handles missing note" $ do
      let (n, ns@(Notes k _)) = mkNote "title" "some text" "example.com"

      getNote k ns `shouldBe` Nothing

  describe "searchNotes" $ do
    it "finds the right note" $ do
      let (n, ns@(Notes k _)) = mkNote "title" "some text" "example.com"
      let xs = searchNotes "text" ns
      xs `shouldBe` [(k - 1)]

    it "doesn't find the wrong note" $ do
      let (n, ns@(Notes k _)) = mkNote "title" "some text" "example.com"
      let xs = searchNotes "wrong" ns
      xs `shouldBe` []
