{-# LANGUAGE OverloadedStrings #-}

module Lib where

import           Data.Map         as M
import           Data.Text        as T
import           Data.Text.IO     as TIO
import           System.Directory (doesFileExist)

type Title = T.Text
type Content = T.Text
type URL = T.Text

data Note = Note {
  title     :: Title
  , content :: Content
  , url     :: URL
  } deriving (Show, Read, Eq)

type NoteId = Int

data Notes = Notes {
  nextKey   :: NoteId
  , noteMap :: M.Map NoteId Note
  } deriving (Show, Read, Eq)

initNotes :: Notes
initNotes = Notes 0 M.empty

readNotes :: FilePath -> IO Notes
readNotes f = do
  fileExists <- doesFileExist f
  case fileExists of
    True -> fmap (read . T.unpack) $ TIO.readFile f
    False -> do
      TIO.putStrLn $ T.pack $ "Note file '" ++ f ++  "' not found, creating empty list of Notes"
      return initNotes

saveNotes :: Notes -> FilePath -> IO ()
saveNotes ns f = TIO.writeFile f $ T.pack $ show ns

addNote :: Notes -> Note -> Notes
addNote (Notes k m) n = Notes (k + 1) (M.insert k n m)

getNote :: NoteId -> Notes -> Maybe Note
getNote k (Notes _ m) = M.lookup k m

deleteNote :: Notes -> NoteId -> Notes
deleteNote (Notes k m) i = Notes i (M.delete i m)

searchNotes :: T.Text -> Notes -> [NoteId]
searchNotes q (Notes _ m) = M.foldrWithKey go [] m where
  go k n@(Note t c u) acc = case q `isInfixOf` t || q `isInfixOf` c || q `isInfixOf` u of
    True -> k:acc
    False -> acc

prettyPrint :: Note -> T.Text
prettyPrint n = T.intercalate ("\n" :: T.Text) [Lib.title n, "------", Lib.content n, Lib.url n, "======"]
